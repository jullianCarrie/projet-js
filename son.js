

const button=document.querySelectorAll('button');
console.log(button);

button.forEach(createEvent);//pour chaque label il va appeler la fonction createEvent-> calback fonction appelé par le forEach

function createEvent(element, index, array){

    element.addEventListener("click", function (event)
    {
        event.preventDefault();
        speak(element);
    });
}

function speak(element){
    let utterance = new SpeechSynthesisUtterance(element.value);
    speechSynthesis.speak(utterance);

}


/* ****************************************** */

const submit=document.getElementById('myBtn');

submit.addEventListener("click", function (event)
{
    event.preventDefault();
    voice();
});

function voice(){
   
    const input=document.querySelector('input[name=voices]');

    if(input.value != "")
    {
        let utterance = new SpeechSynthesisUtterance(input.value);
        const langue = speechSynthesis.getVoices();
        utterance.voice = langue[1];
        speechSynthesis.speak(utterance);
      
        
    }
  

}